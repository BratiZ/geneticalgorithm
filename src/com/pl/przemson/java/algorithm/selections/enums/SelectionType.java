package com.pl.przemson.java.algorithm.selections.enums;

public enum SelectionType {
    ROULETTE_MIN,
    ROULETTE_MAX(true),
    COMPETITIVE_MIN,
    COMPETITIVE_MAX(true),
    TOURNAMENT_MIN,
    TOURNAMENT_MAX(true);

    private boolean isMax;

    SelectionType(boolean isMax) {
        this.isMax = isMax;
    }

    SelectionType() {
        this(false);
    }

    public boolean isMax() {
        return isMax;
    }
}
