package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette.model.ElementWithChanceToSurvive;
import com.pl.przemson.java.algorithm.selections.SelectionStrategy;
import com.pl.przemson.java.utils.PopulationUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public abstract class RouletteBase implements SelectionStrategy {
    private final static Random rnd = new Random();

    protected abstract List<ElementWithChanceToSurvive> changeElementsChangeToVictoryType(List<ElementWithChanceToSurvive> elementsWithChanceToSurvive);

    @Override
    public List<BitsWithFunctionValue> select(List<BitsWithFunctionValue> bitsWithFunctionValues) {
        List<Double> elementsValues = PopulationUtils.getValuesFromPopulation(bitsWithFunctionValues);
        double lowestValue = PopulationUtils.getLowestValueInElements(bitsWithFunctionValues);

        elementsValues = elementsValues
                .stream()
                .map(value -> value + lowestValue)
                .collect(Collectors.toList());

        double elementsValueSum = elementsValues
                .stream()
                .mapToDouble(Double::doubleValue)
                .sum();

        List<ElementWithChanceToSurvive> elementWithChanceToSurvives = bitsWithFunctionValues
                .stream()
                .map(element -> {
                    double cumulativeDistribution = (element.getElementValue() + lowestValue) / elementsValueSum;

                    return new ElementWithChanceToSurvive(element, cumulativeDistribution);
                })
                .sorted()
                .collect(Collectors.toList());

        elementWithChanceToSurvives = changeElementsChangeToVictoryType(elementWithChanceToSurvives);

        List<BitsWithFunctionValue> drawnElements = new ArrayList<>();

        while (drawnElements.size() < bitsWithFunctionValues.size()) {
            double drawValue = rnd.nextDouble();
            double acc = 0;

            for (ElementWithChanceToSurvive elementWithChanceToSurvive : elementWithChanceToSurvives) {
                acc += elementWithChanceToSurvive.getChanceToSurvive();
                if (drawValue <= acc) {
                    drawnElements.add(elementWithChanceToSurvive.getElement());
                    break;
                }
            }
        }

        return drawnElements;
    }
}
