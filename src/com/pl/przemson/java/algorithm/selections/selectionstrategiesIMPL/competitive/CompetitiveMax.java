package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.competitive;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import java.util.Collections;
import java.util.List;

public class CompetitiveMax extends CompetitiveBase {

    @Override
    protected BitsWithFunctionValue drawElementForWinningType(List<BitsWithFunctionValue> controlElements) {
        Collections.sort(controlElements);
        Collections.reverse(controlElements);

        return controlElements.get(0);
    }

    @Override
    public SelectionType getType() {
        return SelectionType.COMPETITIVE_MAX;
    }
}
