package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette;

import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette.model.ElementWithChanceToSurvive;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import java.util.List;

public class RouletteMin extends RouletteBase {

    @Override
    protected List<ElementWithChanceToSurvive> changeElementsChangeToVictoryType(List<ElementWithChanceToSurvive> elementsWithChanceToSurvive) {
        for (int f = 0; f < elementsWithChanceToSurvive.size() / 2; f++) {
            double valueOfFirstPlusFElement = elementsWithChanceToSurvive.get(f).getChanceToSurvive();
            double valueOfLastMinusFElement =
                    elementsWithChanceToSurvive.get(elementsWithChanceToSurvive.size() - f - 1).getChanceToSurvive();

            elementsWithChanceToSurvive.get(f).setChanceToSurvive(valueOfLastMinusFElement);
            elementsWithChanceToSurvive.get(elementsWithChanceToSurvive.size() - f - 1).setChanceToSurvive(valueOfFirstPlusFElement);
        }

        return elementsWithChanceToSurvive;
    }

    @Override
    public SelectionType getType() {
        return SelectionType.ROULETTE_MIN;
    }
}
