package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette;

import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette.model.ElementWithChanceToSurvive;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import java.util.List;

public class RouletteMax extends RouletteBase {

    @Override
    protected List<ElementWithChanceToSurvive> changeElementsChangeToVictoryType(List<ElementWithChanceToSurvive> elementsWithChanceToSurvive) {
        return elementsWithChanceToSurvive;
    }

    @Override
    public SelectionType getType() {
        return SelectionType.ROULETTE_MAX;
    }
}
