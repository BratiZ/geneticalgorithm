package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.competitive;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.selections.SelectionStrategy;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class CompetitiveBase implements SelectionStrategy {
    private final static Random rnd = new Random();

    protected abstract BitsWithFunctionValue drawElementForWinningType(List<BitsWithFunctionValue> controlElements);

    @Override
    public List<BitsWithFunctionValue> select(List<BitsWithFunctionValue> bitsWithFunctionValues) {
        List<BitsWithFunctionValue> drawElements = new ArrayList<>();
        int controlGroupSize = (int) (bitsWithFunctionValues.size() * CONTROL_GROUP_SIZE_MULTIPLIER);

        while (drawElements.size() < bitsWithFunctionValues.size()) {
            List<BitsWithFunctionValue> controlElements = new ArrayList<>();

            for (int f = 0; f < controlGroupSize; f++) {
                controlElements.add(bitsWithFunctionValues.get(rnd.nextInt(bitsWithFunctionValues.size())));
            }

            BitsWithFunctionValue drawElement = drawElementForWinningType(controlElements);

            drawElements.add(drawElement);
        }

        return drawElements;
    }
}
