package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette.model;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;

public class ElementWithChanceToSurvive implements Comparable {
    private BitsWithFunctionValue element;
    private double chanceToSurvive;

    public ElementWithChanceToSurvive(BitsWithFunctionValue element, double chanceToSurvive) {
        this.element = element;
        this.chanceToSurvive = chanceToSurvive;
    }

    public double getChanceToSurvive() {
        return chanceToSurvive;
    }

    public void setChanceToSurvive(double chanceToSurvive) {
        this.chanceToSurvive = chanceToSurvive;
    }

    public BitsWithFunctionValue getElement() {
        return element;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof ElementWithChanceToSurvive)) {
            throw new IllegalArgumentException("Compared objects are not the same: " + o.getClass().getName() + " : " + this.getClass().getName());
        }

        return Double.compare(chanceToSurvive, ((ElementWithChanceToSurvive) o).chanceToSurvive);
    }
}
