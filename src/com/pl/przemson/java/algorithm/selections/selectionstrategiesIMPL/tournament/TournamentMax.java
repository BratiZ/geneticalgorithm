package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.tournament;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TournamentMax extends TournamentBase {

    @Override
    protected List<BitsWithFunctionValue> getSortedElementsByType(List<BitsWithFunctionValue> bitsWithFunctionValues) {
        return bitsWithFunctionValues
                .stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
    }

    @Override
    public SelectionType getType() {
        return SelectionType.TOURNAMENT_MAX;
    }
}
