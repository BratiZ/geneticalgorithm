package com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.tournament;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.selections.SelectionStrategy;
import java.util.ArrayList;
import java.util.List;

public abstract class TournamentBase implements SelectionStrategy {

    abstract protected List<BitsWithFunctionValue> getSortedElementsByType(List<BitsWithFunctionValue> bitsWithFunctionValues);

    @Override
    public List<BitsWithFunctionValue> select(List<BitsWithFunctionValue> bitsWithFunctionValues) {
        List<BitsWithFunctionValue> sortedElements = getSortedElementsByType(bitsWithFunctionValues);

        List<BitsWithFunctionValue> drawnElements = new ArrayList<>();

        while (drawnElements.size() < sortedElements.size()) {
            int index = rnd.nextInt(rnd.nextInt(sortedElements.size()) + 1);

            drawnElements.add(sortedElements.get(index));
        }

        return drawnElements;
    }
}
