package com.pl.przemson.java.algorithm.selections;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import com.pl.przemson.java.core.StrategyInterface;
import java.util.List;
import java.util.Random;

public interface SelectionStrategy extends StrategyInterface<SelectionType> {
    double CONTROL_GROUP_SIZE_MULTIPLIER = 1.0 / 3.0;
    Random rnd = new Random();

    List<BitsWithFunctionValue> select(List<BitsWithFunctionValue> bitsWithFunctionValues);
}
