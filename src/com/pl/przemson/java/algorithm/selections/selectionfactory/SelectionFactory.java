package com.pl.przemson.java.algorithm.selections.selectionfactory;

import com.pl.przemson.java.algorithm.selections.SelectionStrategy;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.competitive.CompetitiveMax;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.competitive.CompetitiveMin;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette.RouletteMax;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.roulette.RouletteMin;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.tournament.TournamentMax;
import com.pl.przemson.java.algorithm.selections.selectionstrategiesIMPL.tournament.TournamentMin;
import com.pl.przemson.java.core.GenericStrategyFactory;

public class SelectionFactory extends GenericStrategyFactory<SelectionType, SelectionStrategy> {

    @Override
    protected void fillMap() {
        addStrategy(new CompetitiveMax());
        addStrategy(new CompetitiveMin());
        addStrategy(new RouletteMax());
        addStrategy(new RouletteMin());
        addStrategy(new TournamentMax());
        addStrategy(new TournamentMin());
    }
}
