package com.pl.przemson.java.algorithm.succession.successionfactory;

import com.pl.przemson.java.algorithm.succession.SuccessionStrategy;
import com.pl.przemson.java.algorithm.succession.enums.SuccessionType;
import com.pl.przemson.java.algorithm.succession.successionstrategiesIMPL.PickBestOfEra;
import com.pl.przemson.java.algorithm.succession.successionstrategiesIMPL.PickRandomOfEra;
import com.pl.przemson.java.algorithm.succession.successionstrategiesIMPL.ReplaceAllPopulations;
import com.pl.przemson.java.core.GenericStrategyFactory;

public class SuccessionFactory extends GenericStrategyFactory<SuccessionType, SuccessionStrategy> {
    @Override
    protected void fillMap() {
        addStrategy(new ReplaceAllPopulations());
        addStrategy(new PickBestOfEra());
        addStrategy(new PickRandomOfEra());
    }
}
