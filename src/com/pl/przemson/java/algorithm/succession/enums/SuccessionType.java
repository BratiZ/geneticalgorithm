package com.pl.przemson.java.algorithm.succession.enums;

public enum SuccessionType {
    REPLACE_ALL_POPULATIONS,
    PICK_BEST_OF_ERA,
    PICK_RANDOM_OF_ERA
}
