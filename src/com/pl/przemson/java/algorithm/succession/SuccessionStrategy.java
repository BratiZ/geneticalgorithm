package com.pl.przemson.java.algorithm.succession;

import com.pl.przemson.java.algorithm.functions.adaptation.AdaptationFunction;
import com.pl.przemson.java.algorithm.functions.rage.RageFunction;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.PopulationArguments;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import com.pl.przemson.java.algorithm.succession.enums.SuccessionType;
import com.pl.przemson.java.core.StrategyInterface;
import java.util.List;

public interface SuccessionStrategy extends StrategyInterface<SuccessionType> {
    List<BitsWithFunctionValue> succession(SelectionType selectionType,
                                           CrossingType crossingType,
                                           List<BitsWithFunctionValue> firsPopulation,
                                           PopulationArguments populationArguments,
                                           AdaptationFunction adaptationFunction,
                                           RageFunction rageFunction);
}
