package com.pl.przemson.java.algorithm.succession.successionstrategiesIMPL;

import com.pl.przemson.java.algorithm.functions.adaptation.AdaptationFunction;
import com.pl.przemson.java.algorithm.functions.rage.RageFunction;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingfactory.CrossingFactory;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.geneticoperations.invertions.Inversion;
import com.pl.przemson.java.algorithm.geneticoperations.mutations.Mutation;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.PopulationArguments;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import com.pl.przemson.java.algorithm.selections.selectionfactory.SelectionFactory;
import com.pl.przemson.java.algorithm.succession.SuccessionStrategy;
import com.pl.przemson.java.algorithm.succession.enums.SuccessionType;
import java.util.List;
import java.util.stream.Collectors;

public class ReplaceAllPopulations implements SuccessionStrategy {

    private SelectionFactory selectionFactory = new SelectionFactory();
    private CrossingFactory crossingFactory = new CrossingFactory();

    @Override
    public List<BitsWithFunctionValue> succession(SelectionType selectionType,
                                                  CrossingType crossingType,
                                                  List<BitsWithFunctionValue> firsPopulation,
                                                  PopulationArguments populationArguments,
                                                  AdaptationFunction adaptationFunction,
                                                  RageFunction rageFunction) {

        List<BitsWithFunctionValue> currentPopulation = firsPopulation;

        for (int f = 0; f < populationArguments.getEras(); f++) {
            currentPopulation = selectionFactory.getStrategy(selectionType).select(currentPopulation);

            currentPopulation = Mutation.mutation(currentPopulation, populationArguments.getPm())
                    .stream()
                    .map(element -> BitsWithFunctionValue.of(element, adaptationFunction, rageFunction))
                    .collect(Collectors.toList());

            currentPopulation = Inversion.inversion(currentPopulation, populationArguments.getPi())
                    .stream()
                    .map(element -> BitsWithFunctionValue.of(element, adaptationFunction, rageFunction))
                    .collect(Collectors.toList());

            currentPopulation = crossingFactory.getStrategy(crossingType).crossElements(currentPopulation,
                    populationArguments.getPk())
                    .stream()
                    .map(element -> BitsWithFunctionValue.of(element, adaptationFunction, rageFunction))
                    .collect(Collectors.toList());
        }

        return currentPopulation;
    }

    @Override
    public SuccessionType getType() {
        return SuccessionType.REPLACE_ALL_POPULATIONS;
    }
}
