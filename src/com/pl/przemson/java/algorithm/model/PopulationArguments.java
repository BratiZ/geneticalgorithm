package com.pl.przemson.java.algorithm.model;

public class PopulationArguments {
    private double pm;
    private double pi;
    private double pk;
    private int li;
    private int eras;

    public PopulationArguments(double pm, double pi, double pk, int li, int eras) {
        this.pm = pm;
        this.pi = pi;
        this.pk = pk;
        this.li = li;
        this.eras = eras;
    }

    public double getPm() {
        return pm;
    }

    public void setPm(double pm) {
        this.pm = pm;
    }

    public double getPi() {
        return pi;
    }

    public void setPi(double pi) {
        this.pi = pi;
    }

    public double getPk() {
        return pk;
    }

    public void setPk(double pk) {
        this.pk = pk;
    }

    public int getLi() {
        return li;
    }

    public void setLi(int li) {
        this.li = li;
    }

    public int getEras() {
        return eras;
    }

    public void setEras(int eras) {
        this.eras = eras;
    }
}
