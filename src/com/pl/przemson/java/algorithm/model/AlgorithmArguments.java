package com.pl.przemson.java.algorithm.model;

import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import com.pl.przemson.java.algorithm.succession.enums.SuccessionType;

public class AlgorithmArguments {
    private final SelectionType selectionType;
    private final SuccessionType successionType;
    private final CrossingType crossingType;

    public AlgorithmArguments(SelectionType selectionType, SuccessionType successionType, CrossingType crossingType) {
        this.selectionType = selectionType;
        this.successionType = successionType;
        this.crossingType = crossingType;
    }

    public SelectionType getSelectionType() {
        return selectionType;
    }

    public SuccessionType getSuccessionType() {
        return successionType;
    }

    public CrossingType getCrossingType() {
        return crossingType;
    }
}
