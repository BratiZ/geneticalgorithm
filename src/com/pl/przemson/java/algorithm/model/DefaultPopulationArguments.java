package com.pl.przemson.java.algorithm.model;

public class DefaultPopulationArguments extends PopulationArguments {
    private static final double pm = 0.1;
    private static final double pi = 0.06;
    private static final double pk = 0.5;
    private static final int li = 10;
    private static final int eras = 10;

    public DefaultPopulationArguments() {
        super(pm, pi, pk, li, eras);
    }
}
