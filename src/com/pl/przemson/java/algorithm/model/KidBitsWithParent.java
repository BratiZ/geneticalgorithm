package com.pl.przemson.java.algorithm.model;

import java.util.List;

public class KidBitsWithParent {
    private List<Integer> kidBits;
    private BitsWithFunctionValue parent;

    public KidBitsWithParent(List<Integer> kidBits, BitsWithFunctionValue parent) {
        this.kidBits = kidBits;
        this.parent = parent;
    }

    public static KidBitsWithParent Of(BitsWithFunctionValue parent) {
        return new KidBitsWithParent(parent.getAllBits(), parent);
    }

    public List<Integer> getKidBits() {
        return kidBits;
    }

    public BitsWithFunctionValue getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "bits: " + kidBits;
    }
}
