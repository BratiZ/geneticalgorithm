package com.pl.przemson.java.algorithm.model;

import com.pl.przemson.java.algorithm.functions.adaptation.AdaptationFunction;
import com.pl.przemson.java.algorithm.functions.rage.RageFunction;
import com.pl.przemson.java.utils.PopulationUtils;
import java.util.ArrayList;
import java.util.List;

public class BitsWithFunctionValue implements Comparable {
    private List<List<Integer>> bits;
    private double elementValue;
    private double a;
    private double b;

    public BitsWithFunctionValue() {
    }

    public BitsWithFunctionValue(List<List<Integer>> bits, double elementValue, double a, double b) {
        this.bits = bits;
        this.elementValue = elementValue;
        this.a = a;
        this.b = b;
    }

    public static BitsWithFunctionValue of(KidBitsWithParent kidBitsWithParent,
                                           AdaptationFunction adaptationFunction,
                                           RageFunction rageFunction) {

        return BitsWithFunctionValue.of(kidBitsWithParent.getKidBits(), kidBitsWithParent.getParent(),
                adaptationFunction, rageFunction);

    }

    public static BitsWithFunctionValue of(List<Integer> newBits,
                                           BitsWithFunctionValue oldElement,
                                           AdaptationFunction adaptationFunction,
                                           RageFunction rageFunction) {

        BitsWithFunctionValue newElement = new BitsWithFunctionValue();
        newElement.setA(oldElement.getA());
        newElement.setB(oldElement.getB());

        List<List<Integer>> newSeparatedBits = new ArrayList<>();
        int startX = 0;

        for (List<Integer> element : oldElement.getBits()) {
            List<Integer> elements = new ArrayList<>();

            for (int f = 0; f < element.size(); f++) {
                elements.add(newBits.get(startX + f));
            }

            startX += element.size() - 1;
            newSeparatedBits.add(elements);
        }

        newElement.setBits(newSeparatedBits);

        List<Integer> listOfConvertedBitsToDec = PopulationUtils.convertFromBinaryListToDecList(newSeparatedBits);
        List<Double> rageChangedValues = new ArrayList<>();

        for (int f = 0; f < listOfConvertedBitsToDec.size(); f++) {
            double x = listOfConvertedBitsToDec.get(f);
            int m = newSeparatedBits.get(f).size();

            rageChangedValues.add(rageFunction.changeRange(oldElement.getA(), oldElement.getB(), m, x));
        }

        newElement.setElementValue(adaptationFunction.countX(rageChangedValues));

        return newElement;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public List<List<Integer>> getBits() {
        return bits;
    }

    public void setBits(List<List<Integer>> bits) {
        this.bits = bits;
    }

    public double getElementValue() {
        return elementValue;
    }

    public void setElementValue(double elementValue) {
        this.elementValue = elementValue;
    }

    public List<Integer> getAllBits() {
        List<Integer> allBits = new ArrayList<>();

        bits.forEach(allBits::addAll);

        return allBits;
    }

    public int getBitsLength() {
        return getAllBits().size();
    }

    @Override
    public String toString() {
        return "bits: " + getAllBits();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof BitsWithFunctionValue)) {
            throw new IllegalArgumentException("Bad object to compare with." + this.getClass().getName() + " != " + o.getClass().getName());
        }

        return Double.compare(elementValue, ((BitsWithFunctionValue) o).elementValue);
    }
}
