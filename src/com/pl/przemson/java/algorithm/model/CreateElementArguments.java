package com.pl.przemson.java.algorithm.model;

public class CreateElementArguments {
    private final int d;
    private final double a;
    private final double b;

    public CreateElementArguments(int d, double a, double b) {
        this.d = d;
        this.a = a;
        this.b = b;
    }

    public int getD() {
        return d;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }
}
