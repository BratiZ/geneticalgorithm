package com.pl.przemson.java.algorithm.functions.rage.rageIMPL;

import com.pl.przemson.java.algorithm.functions.rage.RageFunction;

public class Rage implements RageFunction {
    @Override
    public double changeRange(double a, double b, int m, double x) {
        return a + (b - a) * x / (Math.pow(2, m) - 1);
    }
}
