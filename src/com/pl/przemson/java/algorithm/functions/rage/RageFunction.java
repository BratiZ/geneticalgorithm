package com.pl.przemson.java.algorithm.functions.rage;

public interface RageFunction {
    double changeRange(double a, double b, int m, double x);
}
