package com.pl.przemson.java.algorithm.functions.adaptation.adaptationsIMPL;

import com.pl.przemson.java.algorithm.functions.adaptation.AdaptationFunction;
import java.util.List;

public class Rastrigin implements AdaptationFunction {
    @Override
    public double countX(List<Double> xs, double A, double W) {
        double results = A * xs.size();
        double sum = 0;

        for (int f = 0; f < xs.size(); f++) {
            sum += Math.pow(xs.get(f), 2) - A * Math.cos(W * xs.get(f));
        }

        return results + sum;
    }
}
