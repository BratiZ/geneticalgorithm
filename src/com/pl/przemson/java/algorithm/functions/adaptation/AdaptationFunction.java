package com.pl.przemson.java.algorithm.functions.adaptation;

import java.util.List;

public interface AdaptationFunction {
    double A = 10;
    double W = 20 * Math.PI;

    double countX(List<Double> xs, double A, double W);

    default double countX(List<Double> xs) {
        return countX(xs, A, W);
    }
}
