package com.pl.przemson.java.algorithm;

import com.pl.przemson.java.algorithm.functions.adaptation.AdaptationFunction;
import com.pl.przemson.java.algorithm.functions.rage.RageFunction;
import com.pl.przemson.java.algorithm.model.AlgorithmArguments;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.CreateElementArguments;
import com.pl.przemson.java.algorithm.model.PopulationArguments;
import com.pl.przemson.java.algorithm.succession.successionfactory.SuccessionFactory;
import com.pl.przemson.java.utils.PopulationUtils;
import java.util.ArrayList;
import java.util.List;

public class GenericAlgorithm {
    SuccessionFactory successionFactory = new SuccessionFactory();

    public List<BitsWithFunctionValue> findPopulation(List<CreateElementArguments> elementArguments,
                                                      AlgorithmArguments algorithmArguments,
                                                      PopulationArguments populationArguments,
                                                      AdaptationFunction adaptationFunction,
                                                      RageFunction rageFunction) {

        List<BitsWithFunctionValue> population = createFirstPopulation(elementArguments,
                adaptationFunction,
                rageFunction,
                populationArguments.getLi());

        population = successionFactory.getStrategy(algorithmArguments.getSuccessionType())
                .succession(
                        algorithmArguments.getSelectionType(),
                        algorithmArguments.getCrossingType(),
                        population,
                        populationArguments,
                        adaptationFunction,
                        rageFunction
                );

        return population;
    }

    private List<List<Integer>> GeneratePopulation(int d, double a, double b, int li) {
        List<List<Integer>> dins;

        double combinations = (b - a) * Math.pow(10, d);

        int m = countM(combinations, a, b);

        dins = PopulationUtils.generateMatrix(li, m);

        return dins;
    }

    private int countM(double combinations, double a, double b) {
        int m = 2;
        double bMa = b - a;

        for (int f = 0; (!((Math.pow(bMa, m - 1) < combinations && combinations <= Math.pow(bMa, m)))); m++, f++) ;

        return m;
    }

    private List<BitsWithFunctionValue> createFirstPopulation(List<CreateElementArguments> elementArguments,
                                                              AdaptationFunction adaptationFunction,
                                                              RageFunction rageFunction,
                                                              int li) {
        List<List<Double>> res2 = new ArrayList<>();
        List<BitsWithFunctionValue> BitsWithFunctionValues = new ArrayList<>();
        List<List<List<Integer>>> binaryPopulations = new ArrayList<>();

        if (elementArguments == null || elementArguments.isEmpty()) {
            throw new IllegalArgumentException("Elements arguments are null or empty!");
        }

        for (int f = 0; f < elementArguments.size(); f++) {

            int d = elementArguments.get(f).getD();
            double a = elementArguments.get(f).getA();
            double b = elementArguments.get(f).getB();

            List<List<Integer>> binaryPopulation = GeneratePopulation(d, a, b, li);
            binaryPopulations.add(binaryPopulation);

            List<Integer> decPopulation = PopulationUtils.convertFromBinaryListToDecList(binaryPopulation);
            List<Double> normalizedXs = new ArrayList<>();

            for (int g = 0; g < decPopulation.size(); g++) {
                double x = decPopulation.get(g);
                int m = binaryPopulation.get(g).size();

                normalizedXs.add(rageFunction.changeRange(a, b, m, x));
            }

            res2.add(normalizedXs);
        }

        for (int f = 0; f < res2.get(0).size(); f++) {
            List<Double> elements = new ArrayList<>();
            List<List<Integer>> binaryElements = new ArrayList<>();

            double a = 0;
            double b = 0;

            for (int g = 0; g < res2.size(); g++) {
                a = elementArguments.get(g).getA();
                b = elementArguments.get(g).getB();
                elements.add(res2.get(g).get(f));
                binaryElements.add(binaryPopulations.get(g).get(f));
            }

            double elementsValue = adaptationFunction.countX(elements);
            BitsWithFunctionValues.add(new BitsWithFunctionValue(
                    binaryElements,
                    elementsValue,
                    a,
                    b
            ));
        }

        return BitsWithFunctionValues;
    }
}
