package com.pl.przemson.java.algorithm.geneticoperations.invertions;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.KidBitsWithParent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public abstract class Inversion {
    private static final Random rnd = new Random();

    public static List<KidBitsWithParent> inversion(List<BitsWithFunctionValue> originalPopulation, double pi) {
        return originalPopulation.stream()
                .map(element ->
                        rnd.nextDouble() < pi
                                ? inversion(element)
                                : new KidBitsWithParent(element.getAllBits(), element)
                )
                .collect(Collectors.toList());
    }

    public static KidBitsWithParent inversion(BitsWithFunctionValue bitsWithFunctionValue) {
        List<Integer> allBits = bitsWithFunctionValue.getAllBits();
        List<Integer> allNewBits = new ArrayList<>();
        List<Integer> bitsToSwap = new ArrayList<>();

        int indexStart = rnd.nextInt(allBits.size() - 1);
        int indexEnd = rnd.nextInt(allBits.size() - indexStart) + indexStart;

        for (int f = 0; f < allBits.size(); f++) {
            if (f >= indexStart && f <= indexEnd) {
                bitsToSwap.add(allBits.get(f));
            }
        }

        Collections.reverse(bitsToSwap);

        for (int f = 0, sf = 0; f < allBits.size(); f++) {
            if (f >= indexStart && f <= indexEnd) {
                allNewBits.add(bitsToSwap.get(sf++));
            } else {
                allNewBits.add(allBits.get(f));
            }
        }

        return new KidBitsWithParent(allNewBits, bitsWithFunctionValue);
    }
}
