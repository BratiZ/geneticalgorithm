package com.pl.przemson.java.algorithm.geneticoperations.crossings.model;

import com.pl.przemson.java.core.GenericPair;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;

public class CrossingElementPair extends GenericPair<BitsWithFunctionValue> {
    public CrossingElementPair(BitsWithFunctionValue element1, BitsWithFunctionValue element2) {
        super(element1, element2);
    }

    @Override
    public String toString() {
        return "Element1: " + getElement1().getElementValue() + "Element2: " + getElement2().getElementValue();
    }
}
