package com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL;

import com.pl.przemson.java.algorithm.geneticoperations.crossings.CrossingStrategy;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.model.CrossingElementPair;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.KidBitsWithParent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class CrossingEvenly implements CrossingStrategy {
    private final Random rnd = new Random();

    private List<KidBitsWithParent> evenly(CrossingElementPair pairedElements) {
        BitsWithFunctionValue parent1 = pairedElements.getElement1();
        BitsWithFunctionValue parent2 = pairedElements.getElement2();

        List<Integer> kid1Bits = new ArrayList<>();
        List<Integer> kid2Bits = new ArrayList<>();

        List<Integer> element1Bits = parent1.getAllBits();
        List<Integer> element2Bits = parent2.getAllBits();

        for (int f = 0; f < element1Bits.size(); f++) {

            boolean isChange = rnd.nextBoolean();

            kid1Bits.add(
                    isChange
                            ? element1Bits.get(f)
                            : element2Bits.get(f)
            );

            kid2Bits.add(
                    isChange
                            ? element2Bits.get(f)
                            : element1Bits.get(f)
            );
        }

        return Arrays.asList(
                new KidBitsWithParent(kid1Bits, parent1),
                new KidBitsWithParent(kid2Bits, parent2)
        );
    }

    @Override
    public List<KidBitsWithParent> crossElements(List<BitsWithFunctionValue> bitsWithFunctionValues, double pk) {
        List<BitsWithFunctionValue> elementsThatCanBeCrossed = getElementsThatCanBeCrossed(bitsWithFunctionValues, pk);
        List<CrossingElementPair> pairedElements = createPairs(elementsThatCanBeCrossed);
        List<BitsWithFunctionValue> elementsThatCantBeCrossed = bitsWithFunctionValues.stream()
                .filter(element -> !elementsThatCanBeCrossed.contains(element))
                .collect(Collectors.toList());

        List<KidBitsWithParent> newElements = new ArrayList<>();

        pairedElements.stream()
                .map(this::evenly)
                .forEach(newElements::addAll);

        elementsThatCantBeCrossed.forEach(element -> newElements.add(KidBitsWithParent.Of(element)));
        return newElements;
    }

    @Override
    public CrossingType getType() {
        return CrossingType.EVENLY;
    }
}
