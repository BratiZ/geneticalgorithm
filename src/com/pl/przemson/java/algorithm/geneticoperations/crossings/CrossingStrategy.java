package com.pl.przemson.java.algorithm.geneticoperations.crossings;

import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.model.CrossingElementPair;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.KidBitsWithParent;
import com.pl.przemson.java.core.StrategyInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public interface CrossingStrategy extends StrategyInterface<CrossingType> {
    Random rnd2 = new Random();

    List<KidBitsWithParent> crossElements(List<BitsWithFunctionValue> bitsWithFunctionValues, double pk);

    default List<BitsWithFunctionValue> getElementsThatCanBeCrossed(List<BitsWithFunctionValue> bitsWithFunctionValues, double pk) {
        List<BitsWithFunctionValue> elementsToCross = bitsWithFunctionValues.stream()
                .filter(element -> canCrossElement(pk))
                .collect(Collectors.toList());

        if (elementsToCross.size() % 2 != 0) {
            elementsToCross.remove(rnd2.nextInt(elementsToCross.size()));
        }

        return elementsToCross;
    }

    default List<CrossingElementPair> createPairs(List<BitsWithFunctionValue> elementsToCross) {
        Collections.shuffle(elementsToCross);

        List<CrossingElementPair> pairs = new ArrayList<>();

        for (int f = 0; f < elementsToCross.size(); f += 2) {
            pairs.add(new CrossingElementPair(elementsToCross.get(f), elementsToCross.get(f + 1)));
        }

        return pairs;
    }

    default boolean canCrossElement(double pk) {
        return rnd2.nextDouble() < pk;
    }
}
