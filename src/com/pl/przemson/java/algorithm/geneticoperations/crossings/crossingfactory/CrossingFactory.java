package com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingfactory;

import com.pl.przemson.java.algorithm.geneticoperations.crossings.CrossingStrategy;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL.CrossingByMultiplySpot;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL.CrossingByOneSpot;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL.CrossingByTwoSpot;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL.CrossingEvenly;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.core.GenericStrategyFactory;

public class CrossingFactory extends GenericStrategyFactory<CrossingType, CrossingStrategy> {
    @Override
    protected void fillMap() {
        addStrategy(new CrossingByOneSpot());
        addStrategy(new CrossingByTwoSpot());
        addStrategy(new CrossingByMultiplySpot());
        addStrategy(new CrossingEvenly());
    }
}
