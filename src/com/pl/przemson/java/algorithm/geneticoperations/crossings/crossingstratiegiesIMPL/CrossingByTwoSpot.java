package com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL;

import com.pl.przemson.java.algorithm.geneticoperations.crossings.CrossingStrategy;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.model.CrossingElementPair;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.KidBitsWithParent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CrossingByTwoSpot extends CrossingBase implements CrossingStrategy {
    @Override
    public List<KidBitsWithParent> crossElements(List<BitsWithFunctionValue> bitsWithFunctionValues, double pk) {
        List<BitsWithFunctionValue> elementsThatCanBeCrossed = getElementsThatCanBeCrossed(bitsWithFunctionValues, pk);
        List<CrossingElementPair> pairedElements = createPairs(elementsThatCanBeCrossed);
        List<BitsWithFunctionValue> elementsThatCantBeCrossed = bitsWithFunctionValues.stream()
                .filter(element -> !elementsThatCanBeCrossed.contains(element))
                .collect(Collectors.toList());


        List<KidBitsWithParent> kids = new ArrayList<>();

        int bitsInElement = pairedElements.size() > 0
                ? pairedElements.get(0).getElement1().getBitsLength()
                : 0;

        pairedElements.forEach(pair -> kids.addAll(
                crossByAny(
                        pair.getElement1(),
                        pair.getElement2(),
                        rnd.nextInt(bitsInElement),
                        rnd.nextInt(bitsInElement)
                )));

        elementsThatCantBeCrossed.forEach(element -> kids.add(KidBitsWithParent.Of(element)));
        return kids;
    }

    @Override
    public CrossingType getType() {
        return CrossingType.TWO_SPOTS;
    }
}
