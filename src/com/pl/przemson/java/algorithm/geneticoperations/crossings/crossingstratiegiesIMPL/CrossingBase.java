package com.pl.przemson.java.algorithm.geneticoperations.crossings.crossingstratiegiesIMPL;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.KidBitsWithParent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

abstract public class CrossingBase {
    protected Random rnd = new Random();

    public List<KidBitsWithParent> crossByAny(BitsWithFunctionValue parent1,
                                              BitsWithFunctionValue parent2,
                                              Integer... indexes) {

        List<Integer> parent1Bits = parent1.getAllBits();
        List<Integer> parent2Bits = parent2.getAllBits();

        List<Integer> kid1Bits = new ArrayList<>();
        List<Integer> kid2Bits = new ArrayList<>();

        Arrays.sort(indexes);
        int indexOfChange = 0;

        boolean flip = false;

        for (int f = 0; f < parent1Bits.size(); f++) {
            if (indexOfChange < indexes.length
                    && (
                    !flip && f >= indexes[indexOfChange]
                            || flip && f >= indexes[indexOfChange] + 1
            )
            ) {
                indexOfChange++;
                flip = !flip;
            }

            if (flip) {
                kid1Bits.add(parent2Bits.get(f));
                kid2Bits.add(parent1Bits.get(f));
            } else {
                kid1Bits.add(parent1Bits.get(f));
                kid2Bits.add(parent2Bits.get(f));
            }

        }
        List<KidBitsWithParent> newElements = new ArrayList<>();

        newElements.add(new KidBitsWithParent(kid1Bits, parent1));
        newElements.add(new KidBitsWithParent(kid2Bits, parent2));

        return newElements;
    }
}
