package com.pl.przemson.java.algorithm.geneticoperations.crossings.enums;

public enum CrossingType {
    ONE_SPOT,
    TWO_SPOTS,
    MULTIPLY_SPOTS,
    EVENLY
}
