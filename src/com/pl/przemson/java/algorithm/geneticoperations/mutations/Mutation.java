package com.pl.przemson.java.algorithm.geneticoperations.mutations;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.KidBitsWithParent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public abstract class Mutation {
    private final static Random rnd = new Random();

    public static List<KidBitsWithParent> mutation(List<BitsWithFunctionValue> originalPopulations, double pm) {
        return originalPopulations
                .stream()
                .map(element -> mutation(element, pm))
                .collect(Collectors.toList());
    }

    private static KidBitsWithParent mutation(BitsWithFunctionValue bitsWithFunctionValue, double pm) {
        List<Integer> kidBits = new ArrayList<>();

        //todo - refactor - extract lambda to method
        bitsWithFunctionValue.getBits()
                .stream()
                .map(bits ->
                        bits.stream()
                                .map(bit ->
                                        rnd.nextDouble() >= pm
                                                ? bit
                                                :
                                                bit == 0
                                                        ? 1
                                                        : 0
                                )
                                .collect(Collectors.toList())
                )
                .forEach(kidBits::addAll);

        return new KidBitsWithParent(kidBits, bitsWithFunctionValue);
    }
}
