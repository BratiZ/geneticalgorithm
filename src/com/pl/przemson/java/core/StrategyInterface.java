package com.pl.przemson.java.core;

public interface StrategyInterface<T extends Enum> {
    T getType();
}
