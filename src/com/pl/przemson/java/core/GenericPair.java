package com.pl.przemson.java.core;

public abstract class GenericPair<V> {
    private final V element1;
    private final V element2;

    public GenericPair(V element1, V element2) {
        this.element1 = element1;
        this.element2 = element2;
    }

    public V getElement1() {
        return element1;
    }

    public V getElement2() {
        return element2;
    }

    @Override
    public abstract String toString();
}
