package com.pl.przemson.java.core;

import java.util.HashMap;
import java.util.Map;

public abstract class GenericStrategyFactory<T extends Enum, V extends StrategyInterface<T>> {
    private Map<T, V> strategyMap = new HashMap<>();

    public GenericStrategyFactory() {
        fillMap();
    }

    protected abstract void fillMap();

    protected boolean addStrategy(V value) {
        return this.strategyMap.put(value.getType(), value) != null;
    }

    public V getStrategy(T type) {
        if (!strategyMap.containsKey(type)) {
            throw new IllegalArgumentException("There is no strategy with type: " + type + " in Type: " + type.getClass().getName());
        }

        return strategyMap.get(type);
    }
}
