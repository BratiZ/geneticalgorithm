package com.pl.przemson.java.utils;

import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class PopulationUtils {

    public static List<List<Integer>> generateMatrix(int n, int m) {
        List<List<Integer>> elements = new ArrayList<>();

        Random rnd = new Random();

        for (int f = 0; f < n; f++) {
            elements.add(new ArrayList<>());

            for (int g = 0; g < m; g++) {
                elements.get(f).add(rnd.nextBoolean() ? 0 : 1);
            }
        }
        return elements;
    }

    public static List<Integer> convertFromBinaryListToDecList(List<List<Integer>> elements) {
        List<Integer> newElements = new ArrayList<>();

        elements.forEach(x -> {
            List<Integer> tmpList = new ArrayList<>(x);

            Collections.reverse(tmpList);

            int binary = 0;

            for (int f = 0; f < tmpList.size(); f++) {
                binary += tmpList.get(f) * Math.pow(2, f);
            }

            newElements.add(binary);
        });

        return newElements;
    }

    public static List<Double> getValuesFromPopulation(List<BitsWithFunctionValue> bitsWithFunctionValues) {
        return bitsWithFunctionValues
                .stream()
                .map(BitsWithFunctionValue::getElementValue)
                .collect(Collectors.toList());
    }

    public static double getLowestValueInElements(List<BitsWithFunctionValue> bitsWithFunctionValues) {
        List<Double> elementsValues = getValuesFromPopulation(bitsWithFunctionValues);

        return Math.abs(elementsValues
                .stream()
                .mapToDouble(Double::doubleValue)
                .min()
                .getAsDouble() + 1);

    }
}
