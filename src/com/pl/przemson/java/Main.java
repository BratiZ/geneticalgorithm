package com.pl.przemson.java;

import com.pl.przemson.java.algorithm.GenericAlgorithm;
import com.pl.przemson.java.algorithm.functions.adaptation.AdaptationFunction;
import com.pl.przemson.java.algorithm.functions.adaptation.adaptationsIMPL.Rastrigin;
import com.pl.przemson.java.algorithm.functions.rage.RageFunction;
import com.pl.przemson.java.algorithm.functions.rage.rageIMPL.Rage;
import com.pl.przemson.java.algorithm.geneticoperations.crossings.enums.CrossingType;
import com.pl.przemson.java.algorithm.model.AlgorithmArguments;
import com.pl.przemson.java.algorithm.model.BitsWithFunctionValue;
import com.pl.przemson.java.algorithm.model.CreateElementArguments;
import com.pl.przemson.java.algorithm.model.DefaultPopulationArguments;
import com.pl.przemson.java.algorithm.model.PopulationArguments;
import com.pl.przemson.java.algorithm.selections.enums.SelectionType;
import com.pl.przemson.java.algorithm.succession.enums.SuccessionType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        AdaptationFunction adaptationFunction = new Rastrigin();
        RageFunction rageFunction = new Rage();
        PopulationArguments populationArguments = new DefaultPopulationArguments();
        AlgorithmArguments algorithmArguments = new AlgorithmArguments(
                SelectionType.COMPETITIVE_MAX,
                SuccessionType.PICK_BEST_OF_ERA,
                CrossingType.MULTIPLY_SPOTS
        );

        List<CreateElementArguments> elementArguments = new ArrayList<>();

        for (int f = 0; f < 5; f++) {
            elementArguments.add(new CreateElementArguments(
                    (new Random().nextInt(10) + 1),
                    -1,
                    1
            ));
        }

        GenericAlgorithm genericAlgorithm = new GenericAlgorithm();

//stats

        List<Integer> eras = Arrays.asList(10, 20, 50, 100, 200, 300);
        List<Double> pms = Arrays.asList(0.1, 0.25, 0.5, 0.75, 1.0);
        List<String> csvRes = new ArrayList<>();

        eras.forEach(era -> {
            System.out.println("Era: " + era);
            populationArguments.setEras(era);
            pms.forEach(pm -> {
                System.out.println("\tPm: " + pm);
                populationArguments.setPm(pm);

                long start = System.currentTimeMillis();

                double currentAVG = Double.MAX_VALUE;
                for (int f = 0; f < 100; f++) {
                    double avg = genericAlgorithm.findPopulation(
                            elementArguments,
                            algorithmArguments,
                            populationArguments,
                            adaptationFunction,
                            rageFunction)
                            .stream()
                            .map(BitsWithFunctionValue::getElementValue)
                            .mapToDouble(x -> x)
                            .average()
                            .getAsDouble();

                    if (avg < currentAVG) {
                        currentAVG = avg;
                    }
                }

                long elapsedTime = System.currentTimeMillis() - start;

//                System.out.println("\t\tavg:\n\t\t\t" + Math.round(currentAVG * 10000) / 10000.0);
//                System.out.println("\t\ttime:\n\t\t\t" + elapsedTime / 1000.0);
                csvRes.add(Math.round(currentAVG * 10000) / 10000.0 + "\t" + Math.round(elapsedTime / 10.0) / 100.0);
            });
            csvRes.add("\n");
        });

        System.out.println(String.join("\t", csvRes).replaceAll("\t\n\t", "\n"));
    }
}
